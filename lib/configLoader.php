<?php

class configLoader
{
    private $configFile;
    private $configObject;

    public function __construct($fileLocation, $configData)
    {
        $this->setConfigLocation($fileLocation);
        $this->load();
        $this->validate($configData);
    }

    private function setConfigLocation($fileLocation)
    {
        if(is_file($fileLocation)){
            $this->configFile = $fileLocation;
        }else{
            throw new Exception('Config file not found');
        }
    }

    public function getConfigLocation()
    {
        return $this->configFile;
    }

    private function load()
    {
        $configString = file_get_contents($this->configFile);
        $configObj = json_decode($configString);

        if(is_object($configObj)){
            $this->configObject = $configObj;
        }else{
            throw new Exception('Config file format is invalid');
        }

    }
    
    public function validate(configData $confData)
    {
        if(property_exists($this->configObject, CNF_SOURCE)){
            $confData->setSource($this->configObject->source);
        }else{
            throw new Exception('Config file must have a project source');
        }

        if(property_exists($this->configObject, CNF_DEST)){
            $confData->setDestination($this->configObject->destination);
        }else{
            $confData->setDestination(DIRECTORY_SEPARATOR);
        }

    }
}