<?php
spl_autoload_register(
    function($className)
    {
        $className{0} = strtolower($className{0});
        $classFile = ROOT.'lib/'.$className.'.php';
        if(file_exists($classFile))
          require ($classFile);
        else
          throw new ErrorException('Unable to load '.$className);
    }
);