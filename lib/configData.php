<?php
class configData
{
    private $source;
    private $destination;
    private $destinationType;
    private $destinationLocation;

    private $db;
    private $dbType;
    private $dbLocation;
    private $dbUser;
    private $dbPass;

    private $version;
    private $archive;
    private $archiveLocation;
    private $archiveType;

    /**
     * @param mixed $dbPass
     */
    public function setDbPass($dbPass)
    {
        $this->dbPass = $dbPass;
    }

    /**
     * @return mixed
     */
    public function getDbPass()
    {
        return $this->dbPass;
    }

    /**
     * @param mixed $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return mixed
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * @param mixed $archiveLocation
     */
    public function setArchiveLocation($archiveLocation)
    {
        $this->archiveLocation = $archiveLocation;
    }

    /**
     * @return mixed
     */
    public function getArchiveLocation()
    {
        return $this->archiveLocation;
    }

    /**
     * @param mixed $archiveType
     */
    public function setArchiveType($archiveType)
    {
        $this->archiveType = $archiveType;
    }

    /**
     * @return mixed
     */
    public function getArchiveType()
    {
        return $this->archiveType;
    }

    /**
     * @param mixed $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param mixed $dbLocation
     */
    public function setDbLocation($dbLocation)
    {
        $this->dbLocation = $dbLocation;
    }

    /**
     * @return mixed
     */
    public function getDbLocation()
    {
        return $this->dbLocation;
    }

    /**
     * @param mixed $dbType
     */
    public function setDbType($dbType)
    {
        $this->dbType = $dbType;
    }

    /**
     * @return mixed
     */
    public function getDbType()
    {
        return $this->dbType;
    }

    /**
     * @param mixed $dbUser
     */
    public function setDbUser($dbUser)
    {
        $this->dbUser = $dbUser;
    }

    /**
     * @return mixed
     */
    public function getDbUser()
    {
        return $this->dbUser;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destinationLocation
     */
    public function setDestinationLocation($destinationLocation)
    {
        $this->destinationLocation = $destinationLocation;
    }

    /**
     * @return mixed
     */
    public function getDestinationLocation()
    {
        return $this->destinationLocation;
    }

    /**
     * @param mixed $destinationType
     */
    public function setDestinationType($destinationType)
    {
        $this->destinationType = $destinationType;
    }

    /**
     * @return mixed
     */
    public function getDestinationType()
    {
        return $this->destinationType;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

}